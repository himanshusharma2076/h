from pymongo import MongoClient
from random import randint
import requests
from datetime import datetime, timedelta
import pytz
from dotenv import load_dotenv, dotenv_values


def datetimef(strg):
    dt, _, us = strg.partition(".")
    dt = datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
    timezone = pytz.timezone("Asia/Kolkata")
    date = dt.replace(tzinfo=timezone)
    date = date.strftime("%Y-%m-%d")
    return str(date)


path = "/Users/nishant_gudipaty/Desktop/EY-Intern/EY-DevOps/cred.env"
config = dotenv_values(dotenv_path=path)

client = MongoClient(config["MONGO_DB_PATH"])
db = client[config["DB_NAME"]]

r = requests.get(
    "https://gitlab.com/api/v4/projects/"
    + config["GITLAB_PROJECT_ID"]
    + "/repository/commits",
    headers={"Authorization": config["AUTH_TOKEN"]},
)
print(r.status_code)
json_responses = r.json()
print(len(json_responses))


a = []
for i in range(len(json_responses)):
    a.append(json_responses[i]["id"])

c = []
for i in a:
    path = (
        "https://gitlab.com/api/v4/projects/"
        + config["GITLAB_PROJECT_ID"]
        + "/repository/commits/"
        + str(i)
        + "/statuses"
    )
    r = requests.get(
        path,
        headers={"Authorization": config["AUTH_TOKEN"]},
    )
    alpha1 = r.json()
    c.append(alpha1)

e = []
for i in range(len(c)):
    for data in c[i]:
        dict = {
            "name": data["name"],
            "status": data["status"],
            "date": datetimef(data["created_at"]),
            "sha": data["sha"],
        }
        e.append(dict)


for document in e:
    try:
        result = db[config["COMMIT_STATUS_COLLECTION"]].insert_one(document)
        print(result.inserted_id)
    except:
        pass

db[config["COMMIT_STATUS_COLLECTION"]].create_index("date", unique=True)
