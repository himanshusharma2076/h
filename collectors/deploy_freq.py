from pymongo import MongoClient
from dotenv import load_dotenv, dotenv_values
import requests


path = "/Users/nishant_gudipaty/Desktop/EY-Intern/EY-DevOps/cred.env"
config = dotenv_values(dotenv_path=path)

client = MongoClient(config["MONGO_DB_PATH"])
db = client[config["DB_NAME"]]
mycol = db[config["DEPLOY_FREQ_COLLECTION"]]

response = requests.get(
    "https://gitlab.com/api/v4/projects/"
    + config["GITLAB_PROJECT_ID"]
    + "/dora/metrics?metric=deployment_frequency",  headers={"Authorization": config["AUTH_TOKEN"]}
)

data = response.json()

arr = []
arr2 = []

for i in range(len(data)):
    for k, v in data[i].items():
        if k == "value":
            arr.append(v)
        if k == "date":

            arr2.append(v)

l = []

for i in range(len(arr2)):

    d = {"date": str(arr2[i]), "deployment_frequency": arr[i]}
    try:
        x = mycol.insert_one(d)
        print(x.inserted_id)
    except:
        pass

db[config["DEPLOY_FREQ_COLLECTION"]].create_index("date", unique=True)
