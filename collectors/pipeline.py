import requests
from pymongo import MongoClient
from dotenv import load_dotenv, dotenv_values


path = "/Users/nishant_gudipaty/Desktop/EY-Intern/EY-DevOps/cred.env"
config = dotenv_values(dotenv_path=path)

client = MongoClient(config["MONGO_DB_PATH"])
db = client[config["DB_NAME"]]
mycol = db[config["PIPELINE_COLLECTION"]]


response = requests.get(
    "https://gitlab.com/api/v4/projects/" + config["GITLAB_PROJECT_ID"] + "/pipelines"
)
data = response.json()

arr = []
arr2 = []
d = {}
for i in range(len(data)):
    for k, v in data[i].items():
        if k == "created_at":

            n1 = v[11:13]

            n1 = int(n1)

            n2 = v[8:10]
            n2 = int(n2)
            if n1 > 18:
                n2 += 1
            n3 = v[14:16]
            n3 = int(n3)
            if n1 == 18 and n3 >= 30:
                n2 += 1
            if n2 < 10:
                date = v[0:8] + "0" + str(n2)
            else:
                date = v[0:8] + str(n2)

            arr.append(date)
            d[date] = []

        if k == "status":
            arr2.append(v)

for i in range(len(arr)):
    d[arr[i]].append(arr2[i])

arr3 = []
arr4 = []
for k in d.keys():
    arr3.append(d[k].count("success"))
    arr4.append(d[k].count("canceled") + d[k].count("failed"))

arr5 = []
for i in range(len(arr)):
    if arr[i] not in arr5:
        arr5.append(arr[i])


for i in range(len(arr5)):
    d = {
        "date": str(arr5[i]),
        "successful_triggers": arr4[i],
        "failed/cancelled_triggers": arr3[i],
    }
    try:
        result = mycol.insert_one(d)
        print(result.inserted_id)
    except:
        pass


db[config["PIPELINE_COLLECTION"]].create_index("date", unique=True)