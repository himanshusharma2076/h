from pymongo import MongoClient
import requests
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from dotenv import load_dotenv, dotenv_values
import pandas as pd
import pytz


def datetimef(strg):
    dt, _, us = strg.partition(".")
    dt = datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
    timezone = pytz.timezone("Asia/Kolkata")
    date = dt.replace(tzinfo=timezone)
    date = date.strftime("%Y-%m-%d")
    return str(date)


path = "/Users/nishant_gudipaty/Desktop/EY-Intern/EY-DevOps/cred.env"
config = dotenv_values(dotenv_path=path)

client = MongoClient(config["MONGO_DB_PATH"])
db = client[config["DB_NAME"]]

r = requests.get(
    "https://gitlab.com/api/v4/projects/"
    + config["GITLAB_PROJECT_ID"]
    + "/deployments",
    headers={"Authorization": config["AUTH_TOKEN"]},
)

print(r.status_code)
deployment_data = r.json()


df = pd.DataFrame(columns=["date", "success_deploys", "fail_deploys"])

for data in deployment_data:
    date = datetimef(data["created_at"])

    if data["status"] == "success":
        sd = 1
        fd = 0
    elif data["status"] == "failed":
        sd = 0
        fd = 1
    df_temp = pd.DataFrame(
        [[date, sd, fd]], columns=["date", "success_deploys", "fail_deploys"]
    )
    df = pd.concat([df, df_temp], ignore_index=True)

df_final = df.groupby(["date"], as_index=False).sum()

for id in range(df_final.shape[0]):
    document = {
        "date": df_final.iloc[id]["date"],
        "success_deploys": int(df_final.iloc[id]["success_deploys"]),
        "fail_deploys": int(df_final.iloc[id]["fail_deploys"]),
    }
    try:
        result = db[config["DEPLOYMENTS_COLLECTION"]].insert_one(document)
        print(result.inserted_id)
    except:
        pass

db[config["DEPLOYMENTS_COLLECTION"]].create_index("date", unique=True)
