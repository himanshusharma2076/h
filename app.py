from flask import Flask, jsonify, request, render_template
from pymongo import MongoClient
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from dotenv import dotenv_values
import pandas as pd
import numpy as np


config = dotenv_values("cred.env")

app = Flask(__name__)
client = MongoClient(config["MONGO_DB_PATH"])
db = client[config["DB_NAME"]]
print(db)


######################### COMMITS + COMMIT STATUSES  ###########################


def function(date):
    xdata = []
    ydata_name = []
    ydata_status = []
    sha_key = []
    # commit_data = db.commit_statuses.find()
    commit_data = db[config["COMMIT_STATUS_COLLECTION"]].find()

    for data in commit_data:
        xdata.append(data["date"])
        ydata_name.append(data["name"])
        ydata_status.append(data["status"])
        sha_key.append(data["sha"])

    dict_data = {
        "date": xdata,
        "job": ydata_name,
        "status": ydata_status,
        "sha_keys": sha_key,
    }
    df = pd.DataFrame(dict_data)

    def date_value(date):
        date = str(date)
        df_new = df[df["date"] == date]
        return df_new

    df_new = date_value(date)
    return df_new


@app.route("/commits_graph/<date>", methods=["GET"])
def commit_date(date):
    df = function(date)

    df_group = (
        df.groupby(["job", "status"], as_index=False)
        .count()
        .rename(columns={"date": "count"})
    )
    df_group.drop(["sha_keys"], axis=1, inplace=True)

    text1 = "Commit Statuses of  " + str(date) + " for " + config["GITLAB_PROJECT_NAME"]
    fig = px.bar(
        df_group,
        x="job",
        y="count",
        color="status",
        color_discrete_map={
            "failed": "tomato",
            "canceled": "turquoise",
            "success": "cornflowerblue",
        },
        title=text1,
        text="count",
    )
    fig.update_traces(textposition="outside")
    fig.update_layout(xaxis_tickangle=0)

    fig.write_html("templates/commits_graphs_date.html")
    return render_template("commits_graphs_date.html")


@app.route("/commits_table/<date>", methods=["GET"])
def commit_date_table(date):
    df = function(date)

    text1 = "Commit Statuses of  " + str(date) + " for " + config["GITLAB_PROJECT_NAME"]

    fig = go.Figure(
        data=[
            go.Table(
                header=dict(
                    values=list(df.columns),
                    fill_color="paleturquoise",
                    align="left",
                ),
                cells=dict(
                    values=[df.date, df.job, df.status, df.sha_keys],
                    fill_color="lavender",
                    align="left",
                ),
            )
        ]
    )
    fig.update_layout(title=text1)
    fig.write_html("templates/commits_table_date.html")
    return render_template("commits_table_date.html")


@app.route("/commits_api/<date>", methods=["GET"])
def commits_api_date(date):
    df = function(date)
    output = df.values.tolist()
    return jsonify({"Commit_Statuses": output})


@app.route("/commits_api", methods=["GET"])
def commits_api():
    xdata = []
    ydata_name = []
    ydata_status = []
    sha_key = []
    # commit_data = db.commit_statuses.find()
    commit_data = db[config["COMMIT_STATUS_COLLECTION"]].find()

    for data in commit_data:
        xdata.append(data["date"])
        ydata_name.append(data["name"])
        ydata_status.append(data["status"])
        sha_key.append(data["sha"])

    dict_data = {
        "date": xdata,
        "job": ydata_name,
        "status": ydata_status,
        "sha_keys": sha_key,
    }
    df = pd.DataFrame(dict_data)
    output = df.to_dict("records")
    return jsonify({"Commit_Statuses": output})


######################### DEPLOYMENT FREQUENCY ###########################


@app.route("/deploy_freq_api", methods=["GET"])
def deploy_freq_api():
    # freq = db.deployment_frequency
    freq = db[config["DEPLOY_FREQ_COLLECTION"]]
    output = []

    for q in freq.find():
        output.append(
            {"date": q["date"], "deployment_frequency": q["deployment_frequency"]}
        )
    return jsonify({"deployment_frequencies": output})


@app.route("/deploy_freq_api/<date>", methods=["GET"])
def deploy_freq_date_api(date):
    # freq = db.deployment_frequency
    freq = db[config["DEPLOY_FREQ_COLLECTION"]]
    output = []

    for q in freq.find({"date": date}):
        output.append(
            {"date": q["date"], "deployment_frequency": q["deployment_frequency"]}
        )

    return jsonify({"deployment_frequencies": output})


@app.route("/deploy_freq", methods=["GET"])
def deploy_freq_graph():
    # freq = db.deployment_frequency
    freq = db[config["DEPLOY_FREQ_COLLECTION"]]

    output_data = []
    output_freq = []

    for q in freq.find():
        output_data.append(q["date"])
        output_freq.append(int(q["deployment_frequency"]))

    output = np.c_[output_data, output_freq]
    df = pd.DataFrame(output, columns=["date", "deployment_frequency"])

    fig = px.bar(
        df,
        x="date",
        y="deployment_frequency",
        title="Deployment Frequency " + " for " + config["GITLAB_PROJECT_NAME"],
        hover_data=["deployment_frequency"],
        text="deployment_frequency",
    )
    fig.update_traces(textposition="outside")
    fig.update_layout(xaxis_tickangle=45)

    fig.write_html("templates/deploy_freq.html")
    return render_template("deploy_freq.html")


######################### PIPELINE ###########################


@app.route("/pipeline_api", methods=["GET"])
def pipeline_api():
    # freq = db.pipeline
    freq = db[config["PIPELINE_COLLECTION"]]
    output = []

    for q in freq.find():
        output.append(
            {
                "date": q["date"],
                "successful_triggers": q["successful_triggers"],
                "failed_triggers": q["failed/cancelled_triggers"],
            }
        )
    return jsonify({"pipelines": output})


@app.route("/pipeline_api/<date>", methods=["GET"])
def pipeline_date_api(date):
    # freq = db.pipeline
    freq = db[config["PIPELINE_COLLECTION"]]
    output = []

    for q in freq.find({"date": date}):
        output.append(
            {
                "date": q["date"],
                "successful_triggers": q["successful_triggers"],
                "failed_triggers": q["failed/cancelled_triggers"],
            }
        )

    return jsonify({"pipelines": output})


@app.route("/pipeline", methods=["GET"])
def pipeline():
    # freq = db.pipeline
    freq = db[config["PIPELINE_COLLECTION"]]

    output = []
    for q in freq.find():
        output.append(
            {
                "date": q["date"],
                "successful_triggers": q["successful_triggers"],
                "failed_triggers": q["failed/cancelled_triggers"],
            }
        )

    df = pd.DataFrame(output)

    colors = {
        "success_triggers": "cornflowerblue",
        "fail_triggers": "tomato",
    }

    fig = go.Figure(
        data=[
            go.Bar(
                name="Success",
                x=list(df["date"].values),
                y=list(df["successful_triggers"].values),
                marker_color=colors["success_triggers"],
            ),
            go.Bar(
                name="Fails",
                x=list(df["date"].values),
                y=list(df["failed_triggers"].values),
                marker_color=colors["fail_triggers"],
            ),
        ]
    )
    fig.update_layout(
        barmode="stack",
        title_text="Pipeline Information" + " for " + config["GITLAB_PROJECT_NAME"],
    )

    fig.write_html("templates/pipeline.html")
    return render_template("pipeline.html")


######################### DEPLOYMENTS ###########################


@app.route("/deploys", methods=["GET"])
def deploy_all():
    # deploy_data = db.deployments.find()
    deploy_data = db[config["DEPLOYMENTS_COLLECTION"]].find()

    xdata = []
    ydata_success = []
    ydata_fails = []
    for data in deploy_data:
        xdata.append(data["date"])
        ydata_success.append(data["success_deploys"])
        ydata_fails.append(data["fail_deploys"])

    colors = {
        "success": "cornflowerblue",
        "fails": "tomato",
    }

    fig = go.Figure(
        data=[
            go.Bar(
                name="Success", x=xdata, y=ydata_success, marker_color=colors["success"]
            ),
            go.Bar(name="Fails", x=xdata, y=ydata_fails, marker_color=colors["fails"]),
        ]
    )
    fig.update_layout(
        barmode="stack",
        title_text="Deploys Information for " + config["GITLAB_PROJECT_NAME"],
    )

    fig.write_html("templates/deploys.html")
    return render_template("deploys.html")


@app.route("/deploys_api/", methods=["GET"])
def deploy_api():
    # deploy_data = db.deployments.find()
    deploy_data = db[config["DEPLOYMENTS_COLLECTION"]].find()
    output = []

    for data in deploy_data:
        output.append(
            {
                "date": data["date"],
                "success_deploys": data["success_deploys"],
                "fail_deploys": data["fail_deploys"],
            }
        )

    return jsonify({"deploys": output})


@app.route("/deploys/<date>", methods=["GET"])
def deploy_date_api(date):
    deploy_data = db[config["DEPLOYMENTS_COLLECTION"]].find({"date": date})
    output = []

    for data in deploy_data:
        output.append(
            {
                "date": data["date"],
                "success_deploys": data["success_deploys"],
                "fail_deploys": data["fail_deploys"],
            }
        )

    return jsonify({"deploys": output})


# app.run(host="localhost", port=5000, debug=True)
app.run(host=config["HOST"], port=config["PORT"], debug=True)
